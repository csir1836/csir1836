package salariati.main;

import salariati.controller.EmployeeController;
import salariati.enumeration.DidacticFunction;
import salariati.model.Employee;
import salariati.repository.implementations.IEmployeeRepositoryImpl;
import salariati.repository.interfaces.IEmployeeRepository;

import java.util.List;
import java.util.Scanner;

public class StartApp {


    private static void listaMeniu() {
        System.out.println("1. Adauga un nou angajat");
        System.out.println("2. Modifica functia didactica");
        System.out.println("3. Afisarea salariatilor ordonati descrescator dupa salariu si crescator dupa varsta (CNP)");
    }

    private static void listaFunctii() {
        System.out.println("1.ASISTENT");
        System.out.println("2.LECTURER");
        System.out.println("3.TEACHER");
        System.out.println("4.CONFERENTIAR");
    }

    public static void main(String[] args) {
        IEmployeeRepository employeeRepository = new IEmployeeRepositoryImpl();
        EmployeeController employeeController = new EmployeeController(employeeRepository);


//        Employee employee2 = new Employee("paul","1234567890765",DidacticFunction.ASISTENT,"12345");
//        Employee employee3 = new Employee("gheorghe","1234567890765",DidacticFunction.ASISTENT,"12345");
//        employeesRepository.modifyEmployee(employee2,employee3);


        boolean afisareMeniu = true;
        listaMeniu();
        while (afisareMeniu) {
            System.out.print("Alege optiunea: ");
            Scanner in = new Scanner(System.in);
            int optiune = in.nextInt();
            in.nextLine();
            if (optiune == 1) {
                System.out.println("RESTRICITE: Numarul de caractere al campurilor trebuie sa fie: lastName > 2, cnp = 13, salary > 1 si suma pozitiva");
                Employee employee = new Employee();
                System.out.print("firstName= ");
                String firstName = in.nextLine();
                employee.setFirstName(firstName);

                System.out.print("lastName= ");
                String lastName = in.nextLine();
                employee.setLastName(lastName);
                System.out.println();
                System.out.print("cnp= ");
                String cnp = in.nextLine();
                employee.setCnp(cnp);
                System.out.println();
                System.out.println("Alege functia didactica:");
                listaFunctii();
                System.out.print("Numarul optiunii: ");
                int functie = in.nextInt();
                in.nextLine();
                boolean check = false;
                if (functie == 1) {
                    employee.setFunction(DidacticFunction.ASISTENT);
                    check = true;
                }
                if (functie == 2) {
                    employee.setFunction(DidacticFunction.LECTURER);
                    check = true;
                }
                if (functie == 3) {
                    employee.setFunction(DidacticFunction.TEACHER);
                    check = true;
                }
                if (functie == 4) {
                    employee.setFunction(DidacticFunction.CONFERENTIAR);
                    check = true;
                }
                if (check == false) {
                    System.out.println("Alege 1/2/3 !");
                    continue;
                }
                System.out.println();
                System.out.print("salary= ");
                String salary = in.nextLine();
                employee.setSalary(salary);
                if (employeeController.addEmployee(employee) == true) {
                    System.out.println("Salvare cu succes!");
                } else {
                    System.out.println("Probleme de validare!");
                }

            }
            if (optiune == 2) {
                Employee oldEmployee = new Employee();
                Employee newEmploye = new Employee();
                System.out.print("Update la user-ul cu CNP= ");
                String cnp = in.nextLine();
                oldEmployee.setCnp(cnp);
                System.out.println();
                System.out.println("Alege functia didactica:");
                listaFunctii();
                System.out.print("Numarul optiunii: ");
                int functie = in.nextInt();
                in.nextLine();
                boolean check = false;
                if (functie == 1) {
                    newEmploye.setFunction(DidacticFunction.ASISTENT);
                    check = true;
                }
                if (functie == 2) {
                    newEmploye.setFunction(DidacticFunction.LECTURER);
                    check = true;
                }
                if (functie == 3) {
                    newEmploye.setFunction(DidacticFunction.TEACHER);
                    check = true;
                }
                if (functie == 4) {
                    newEmploye.setFunction(DidacticFunction.CONFERENTIAR);
                    check = true;
                }
                if (check == false) {
                    System.out.println("Alege 1/2/3 !");
                    continue;
                }
                System.out.println();
                employeeController.modifyEmployee(oldEmployee, newEmploye);
            }
            if (optiune == 3) {
                List<Employee> sort = employeeController.sort(employeeController.getEmployeesList());

                for (Employee employee : sort) {
                    System.out.println(employee);
                }

            }
            if (optiune == 0) {
                afisareMeniu = false;
            }
        }
    }

}
