package salariati.exception;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class EmployeeException extends Exception {

    private static final long serialVersionUID = 1460943029226542682L;

    public EmployeeException() {
    }

    public EmployeeException(String message) {
        super(message);
    }

}
