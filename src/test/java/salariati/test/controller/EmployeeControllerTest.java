package salariati.test.controller;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import salariati.controller.EmployeeController;
import salariati.model.Employee;
import salariati.repository.implementations.IEmployeeRepositoryImpl;
import salariati.repository.interfaces.IEmployeeRepository;

import java.util.Arrays;
import java.util.List;

import static java.lang.Boolean.TRUE;
import static salariati.enumeration.DidacticFunction.ASISTENT;
import static salariati.enumeration.DidacticFunction.CONFERENTIAR;
import static salariati.enumeration.DidacticFunction.LECTURER;

public class EmployeeControllerTest {
    private IEmployeeRepository employeeRepository;
    private EmployeeController employeeController;

    @Before
    public void setUp(){
        employeeRepository = new IEmployeeRepositoryImpl();
        employeeController = new EmployeeController(employeeRepository);
    }

    //addEmployee
    @Test
    public void shouldReturnTrueForValidEmployee(){
        Employee employee = new Employee("Moldovan","Maria","2921221375121", ASISTENT, "1000");
        Assert.assertEquals(employee.getFirstName(), "Moldovan");
        Assert.assertEquals(employee.getLastName(), "Maria");
        Assert.assertEquals(employee.getCnp(), "2921221375121");
        Assert.assertEquals(employee.getFunction(), ASISTENT);
        Assert.assertEquals(employee.getSalary(), "1000");

        boolean result = employeeController.addEmployee(employee);
        Assert.assertTrue(result);

        employeeRepository.deleteEmployee(employee);
    }

    @Test
    public void shouldFailForInvalidCNPEmployee(){
        Employee employee = new Employee("Pop","Gabriel","2921221375121234", ASISTENT, "2900");
        Assert.assertEquals(employee.getFirstName(), "Pop");
        Assert.assertEquals(employee.getLastName(), "Gabriel");
        Assert.assertEquals(employee.getCnp(), "2921221375121234");
        Assert.assertEquals(employee.getFunction(), ASISTENT);
        Assert.assertEquals(employee.getSalary(), "2900");

        boolean result = employeeController.addEmployee(employee);
        Assert.assertFalse(result);

    }

    @Test
    public void shouldFailForInvalidCNPStringEmployee(){
        Employee employee = new Employee("Rus","Silviu","tb345546ngnhg", ASISTENT, "2900");
        Assert.assertEquals(employee.getFirstName(), "Rus");
        Assert.assertEquals(employee.getLastName(), "Silviu");
        Assert.assertEquals(employee.getCnp(), "tb345546ngnhg");
        Assert.assertEquals(employee.getFunction(), ASISTENT);
        Assert.assertEquals(employee.getSalary(), "2900");

        boolean result = employeeController.addEmployee(employee);
        Assert.assertFalse(result);

    }

    @Test
    public void shouldFailForInvalidCNPLengthNot13Employee(){
        Employee employee = new Employee("Pop","Gabriel","12345", ASISTENT, "111");
        Assert.assertEquals(employee.getFirstName(), "Pop");
        Assert.assertEquals(employee.getLastName(), "Gabriel");
        Assert.assertEquals(employee.getCnp(), "12345");
        Assert.assertEquals(employee.getFunction(), ASISTENT);
        Assert.assertEquals(employee.getSalary(), "111");

        boolean result = employeeController.addEmployee(employee);
        Assert.assertFalse(result);

    }

    @Test
    public void shouldFailForInvalidCNPLengthNot13Employee2(){
        Employee employee = new Employee("Pop","Gabriel","123456789012345", ASISTENT, "111");
        Assert.assertEquals(employee.getFirstName(), "Pop");
        Assert.assertEquals(employee.getLastName(), "Gabriel");
        Assert.assertEquals(employee.getCnp(), "123456789012345");
        Assert.assertEquals(employee.getFunction(), ASISTENT);
        Assert.assertEquals(employee.getSalary(), "111");

        boolean result = employeeController.addEmployee(employee);
        Assert.assertFalse(result);

    }

    @Test
    public void shouldFailForInvalidSalaryEmployee(){
        Employee employee = new Employee("Pop","Gabriel","2921221375121234", ASISTENT, "2%%900");
        Assert.assertEquals(employee.getFirstName(), "Pop");
        Assert.assertEquals(employee.getLastName(), "Gabriel");
        Assert.assertEquals(employee.getCnp(), "2921221375121234");
        Assert.assertEquals(employee.getFunction(), ASISTENT);
        Assert.assertEquals(employee.getSalary(), "2%%900");

        boolean result = employeeController.addEmployee(employee);
        Assert.assertFalse(result);
    }

    @Test
    public void shouldFailForEmptySalaryEmployee(){
        Employee employee = new Employee("Pop","Gabriel","2921221375121234", ASISTENT, "");
        Assert.assertEquals(employee.getFirstName(), "Pop");
        Assert.assertEquals(employee.getLastName(), "Gabriel");
        Assert.assertEquals(employee.getCnp(), "2921221375121234");
        Assert.assertEquals(employee.getFunction(), ASISTENT);
        Assert.assertEquals(employee.getSalary(), "");

        boolean result = employeeController.addEmployee(employee);
        Assert.assertFalse(result);
    }


    @Test
    public void shouldFailForEmptyNameEmployee(){
        Employee employee = new Employee("","Gabriel","2921221375121234", ASISTENT, "1234");
        Assert.assertEquals(employee.getFirstName(), "");
        Assert.assertEquals(employee.getLastName(), "Gabriel");
        Assert.assertEquals(employee.getCnp(), "2921221375121234");
        Assert.assertEquals(employee.getFunction(), ASISTENT);
        Assert.assertEquals(employee.getSalary(), "1234");

        boolean result = employeeController.addEmployee(employee);
        Assert.assertFalse(result);
    }

    @Test(expected = NullPointerException.class)
    public void shouldFailForInvalidFunctionEmployee(){
        Employee employee = new Employee("Pop","Gabriel","2921221375121234", null, "2%%900");
        Assert.assertEquals(employee.getFirstName(), "Pop");
        Assert.assertEquals(employee.getLastName(), "Gabriel");
        Assert.assertEquals(employee.getCnp(), "2921221375121234");
        Assert.assertEquals(employee.getFunction(), null);
        Assert.assertEquals(employee.getSalary(), "2%%900");

        boolean result = employeeController.addEmployee(employee);
        Assert.assertFalse(result);
    }

    //modify employee
    @Test
    public void shouldModifyEmployee(){
        Employee oldEmployee = new Employee("Rete","Mihaela","2921221375121", CONFERENTIAR, "1234");
        Assert.assertEquals(oldEmployee.getFirstName(), "Rete");
        Assert.assertEquals(oldEmployee.getLastName(), "Mihaela");
        Assert.assertEquals(oldEmployee.getCnp(), "2921221375121");
        Assert.assertEquals(oldEmployee.getFunction(), CONFERENTIAR);
        Assert.assertEquals(oldEmployee.getSalary(), "1234");

        boolean result = employeeController.addEmployee(oldEmployee);
        Assert.assertTrue(result);

        Employee newEmployee = new Employee("Rete","Mihaela","2921221375121", ASISTENT, "1234");
        Assert.assertEquals(newEmployee.getFunction(), ASISTENT);

        employeeController.modifyEmployee(oldEmployee, newEmployee);

        List<Employee> employees = employeeController.getEmployeesList();
        for(Employee employee:employees){
            if(employee.equals(newEmployee)){
                Assert.assertTrue("modificat cu success", TRUE);
            }
        }
        employeeRepository.deleteEmployee(newEmployee);
    }

    @Test
    public void shouldModifyEmployee2(){
        Employee oldEmployee = new Employee("Moldovan","Maria","2921221375121", CONFERENTIAR, "1000");
        Assert.assertEquals(oldEmployee.getFirstName(), "Moldovan");
        Assert.assertEquals(oldEmployee.getLastName(), "Maria");
        Assert.assertEquals(oldEmployee.getCnp(), "2921221375121");
        Assert.assertEquals(oldEmployee.getFunction(), CONFERENTIAR);
        Assert.assertEquals(oldEmployee.getSalary(), "1000");

        boolean result = employeeController.addEmployee(oldEmployee);
        Assert.assertTrue(result);

        Employee newEmployee = new Employee("Moldovan","Maria","2921221375121", LECTURER, "1000");
        Assert.assertEquals(newEmployee.getFunction(), LECTURER);

        employeeController.modifyEmployee(oldEmployee, newEmployee);

        List<Employee> employees = employeeController.getEmployeesList();
        for(Employee employee:employees){
            if(employee.equals(newEmployee)){
                Assert.assertTrue("modificat cu success", TRUE);
            }
        }
        employeeRepository.deleteEmployee(newEmployee);
    }

    @Test(expected = NullPointerException.class)
    public void shouldFailModifyEmployee(){
        Employee oldEmployee = new Employee("Rete","Mihaela","2921221375121", CONFERENTIAR, "1234");
        boolean result = employeeController.addEmployee(oldEmployee);
        Assert.assertTrue(result);

        Employee newEmployee = new Employee("Rete","Mihaela","2921221375121", null, "1234");

        employeeController.modifyEmployee(oldEmployee, newEmployee);
    }

    @Test
    public void shouldBeOrdered(){
        Employee employee1 = new Employee("Rete","Mihaela","2911002356881", CONFERENTIAR, "1000");
        Employee employee2 = new Employee("Marian","Adela","2896656775656", ASISTENT, "900");
        Employee employee3 = new Employee("Popovici","Matei","1876565765656", LECTURER, "300");

        List<Employee> orderedList = Arrays.asList(employee1, employee2, employee3);
        List<Employee> unorderedList = Arrays.asList(employee2, employee3, employee1);
        employeeController.sort(unorderedList);
        Assert.assertEquals(orderedList,unorderedList);
    }

    @Test(expected = NumberFormatException.class)
    public void shouldBeUnOrdered(){
        Employee employee1 = new Employee("Rete","Mihaela","2911002356881", CONFERENTIAR, "10%00");
        Employee employee2 = new Employee("Marian","Adela","2896656775656", ASISTENT, "90%0");
        Employee employee3 = new Employee("Popovici","Matei","1876565765656", LECTURER, "300");

        List<Employee> unorderedList = Arrays.asList(employee2, employee3, employee1);
        employeeController.sort(unorderedList);
    }
}
