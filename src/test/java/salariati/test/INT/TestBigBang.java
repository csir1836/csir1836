package salariati.test.INT;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import salariati.controller.EmployeeController;
import salariati.model.Employee;
import salariati.repository.implementations.IEmployeeRepositoryImpl;
import salariati.repository.interfaces.IEmployeeRepository;

import java.util.Arrays;
import java.util.List;

import static java.lang.Boolean.TRUE;
import static salariati.enumeration.DidacticFunction.ASISTENT;
import static salariati.enumeration.DidacticFunction.CONFERENTIAR;
import static salariati.enumeration.DidacticFunction.LECTURER;

public class TestBigBang {
    private IEmployeeRepository employeeRepository;
    private EmployeeController employeeController;

    @Before
    public void setUp() {
        employeeRepository = new IEmployeeRepositoryImpl();
        employeeController = new EmployeeController(employeeRepository);
    }

    @Test
    public void testModulA(){
        Employee employee = new Employee("Pop", "Andreea", "2921221375121", ASISTENT, "1000");
        Assert.assertEquals(employee.getFirstName(), "Pop");
        Assert.assertEquals(employee.getLastName(), "Andreea");
        Assert.assertEquals(employee.getCnp(), "2921221375121");
        Assert.assertEquals(employee.getFunction(), ASISTENT);
        Assert.assertEquals(employee.getSalary(), "1000");

        boolean result = employeeController.addEmployee(employee);
        Assert.assertTrue(result);

        employeeRepository.deleteEmployee(employee);
    }

    @Test
    public void testModulB(){
        Employee oldEmployee = new Employee("Buzgar","Mihai","1921221375121", CONFERENTIAR, "1234");
        Assert.assertEquals(oldEmployee.getFirstName(), "Buzgar");
        Assert.assertEquals(oldEmployee.getLastName(), "Mihai");
        Assert.assertEquals(oldEmployee.getCnp(), "1921221375121");
        Assert.assertEquals(oldEmployee.getFunction(), CONFERENTIAR);
        Assert.assertEquals(oldEmployee.getSalary(), "1234");

        boolean result = employeeController.addEmployee(oldEmployee);
        Assert.assertTrue(result);

        Employee newEmployee = new Employee("Buzgar","Mihai","1921221375121", ASISTENT, "1234");
        Assert.assertEquals(newEmployee.getFunction(), ASISTENT);

        employeeController.modifyEmployee(oldEmployee, newEmployee);

        List<Employee> employees = employeeController.getEmployeesList();
        for(Employee employee:employees){
            if(employee.equals(newEmployee)){
                Assert.assertTrue("modificat cu success", TRUE);
            }
        }
        employeeRepository.deleteEmployee(newEmployee);
    }

    @Test
    public void testModulC(){
        Employee employee1 = new Employee("Caspriac","Dalia","2911002356881", CONFERENTIAR, "1000");
        Employee employee2 = new Employee("Marian","Adela","2896656775656", ASISTENT, "900");
        Employee employee3 = new Employee("Chiorean","Adrian","1876565765656", LECTURER, "300");

        List<Employee> orderedList = Arrays.asList(employee1, employee2, employee3);
        List<Employee> unorderedList = Arrays.asList(employee2, employee3, employee1);
        employeeController.sort(unorderedList);
        Assert.assertEquals(orderedList,unorderedList);
    }

    @Test
    public void testIntegrareABC(){
        Employee employee1 = new Employee("Caspriac","Dalia","2911002356881", CONFERENTIAR, "1000");
        Employee employee2 = new Employee("Marian","Adela","2896656775656", ASISTENT, "900");
        Employee employee3 = new Employee("Chiorean","Adrian","1876565765656", LECTURER, "300");

        //test modul A
        Assert.assertTrue(employeeController.addEmployee(employee1));
        Assert.assertTrue(employeeController.addEmployee(employee2));
        Assert.assertTrue(employeeController.addEmployee(employee3));

        //test modul B
        Employee newEmployee = new Employee("Buzgar","Mihai","1876565765656", LECTURER, "300");
        Assert.assertEquals(newEmployee.getFunction(), LECTURER);

        employeeController.modifyEmployee(employee3, newEmployee);

        List<Employee> employees = employeeController.getEmployeesList();
        for(Employee employee:employees){
            if(employee.equals(newEmployee)){
                Assert.assertTrue("modificat cu success", TRUE);
            }
        }

        //test modul C
        List<Employee> orderedList = Arrays.asList(employee1, employee2, newEmployee);
        List<Employee> unorderedList = Arrays.asList(employee2, newEmployee, employee1);
        employeeController.sort(unorderedList);
        Assert.assertEquals(orderedList,unorderedList);

        employeeRepository.deleteEmployee(employee1);
        employeeRepository.deleteEmployee(employee2);
        employeeRepository.deleteEmployee(employee3);
        employeeRepository.deleteEmployee(newEmployee);
    }
}
