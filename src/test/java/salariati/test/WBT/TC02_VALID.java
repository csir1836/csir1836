package salariati.test.WBT;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import salariati.controller.EmployeeController;
import salariati.model.Employee;
import salariati.repository.implementations.IEmployeeRepositoryImpl;
import salariati.repository.interfaces.IEmployeeRepository;

import java.util.List;

import static java.lang.Boolean.TRUE;
import static salariati.enumeration.DidacticFunction.ASISTENT;
import static salariati.enumeration.DidacticFunction.CONFERENTIAR;

public class TC02_VALID {
    private IEmployeeRepository employeeRepository;
    private EmployeeController employeeController;

    @Before
    public void setUp() {
        employeeRepository = new IEmployeeRepositoryImpl();
        employeeController = new EmployeeController(employeeRepository);
    }

    @Test
    public void shouldModifyEmployee(){
        Employee oldEmployee = new Employee("Colac","Rares","1921221375121", CONFERENTIAR, "1234");
        Assert.assertEquals(oldEmployee.getFirstName(), "Colac");
        Assert.assertEquals(oldEmployee.getLastName(), "Rares");
        Assert.assertEquals(oldEmployee.getCnp(), "1921221375121");
        Assert.assertEquals(oldEmployee.getFunction(), CONFERENTIAR);
        Assert.assertEquals(oldEmployee.getSalary(), "1234");

        boolean result = employeeController.addEmployee(oldEmployee);
        Assert.assertTrue(result);

        Employee newEmployee = new Employee("Colac","Rares","1921221375121", ASISTENT, "1234");
        Assert.assertEquals(newEmployee.getFunction(), ASISTENT);

        employeeController.modifyEmployee(oldEmployee, newEmployee);

        List<Employee> employees = employeeController.getEmployeesList();
        for(Employee employee:employees){
            if(employee.equals(newEmployee)){
                Assert.assertTrue("modificat cu success", TRUE);
            }
        }
        employeeRepository.deleteEmployee(newEmployee);
    }
}
