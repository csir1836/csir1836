package salariati.test.WBT;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import salariati.controller.EmployeeController;
import salariati.model.Employee;
import salariati.repository.implementations.IEmployeeRepositoryImpl;
import salariati.repository.interfaces.IEmployeeRepository;

import static salariati.enumeration.DidacticFunction.CONFERENTIAR;

public class TC02_INVALID {
    private IEmployeeRepository employeeRepository;
    private EmployeeController employeeController;

    @Before
    public void setUp() {
        employeeRepository = new IEmployeeRepositoryImpl();
        employeeController = new EmployeeController(employeeRepository);
    }

    @Test
    public void employeeShouldNotBeModified2() {
        Employee oldEmployee = new Employee("Caspriac", "Dalia", "2921221375121", CONFERENTIAR, "1234");
        Assert.assertEquals(oldEmployee.getFirstName(), "Caspriac");
        Assert.assertEquals(oldEmployee.getLastName(), "Dalia");
        Assert.assertEquals(oldEmployee.getCnp(), "2921221375121");
        Assert.assertEquals(oldEmployee.getFunction(), CONFERENTIAR);
        Assert.assertEquals(oldEmployee.getSalary(), "1234");

        boolean result = employeeController.addEmployee(oldEmployee);
        Assert.assertTrue(result);

        Employee newEmployee = new Employee("Caspriac", "Dalia", "2921241375121", CONFERENTIAR, "1234");
        Assert.assertEquals(newEmployee.getFunction(), CONFERENTIAR);

        employeeController.modifyEmployee(oldEmployee, newEmployee);
        employeeRepository.deleteEmployee(newEmployee);
    }
}
