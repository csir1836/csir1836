package salariati.test.BBT;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import salariati.controller.EmployeeController;
import salariati.model.Employee;
import salariati.repository.implementations.IEmployeeRepositoryImpl;
import salariati.repository.interfaces.IEmployeeRepository;

import static salariati.enumeration.DidacticFunction.ASISTENT;

public class TC01_ECP_INVALID {
    private IEmployeeRepository employeeRepository;
    private EmployeeController employeeController;

    @Before
    public void setUp() {
        employeeRepository = new IEmployeeRepositoryImpl();
        employeeController = new EmployeeController(employeeRepository);
    }

    @Test
    public void shouldFailForInvalidCNPEmployee() {
        Employee employee = new Employee("Mihali", "Gabriel", "2921221375121234", ASISTENT, "2900");
        Assert.assertEquals(employee.getFirstName(), "Mihali");
        Assert.assertEquals(employee.getLastName(), "Gabriel");
        Assert.assertEquals(employee.getCnp(), "2921221375121234");
        Assert.assertEquals(employee.getFunction(), ASISTENT);
        Assert.assertEquals(employee.getSalary(), "2900");

        boolean result = employeeController.addEmployee(employee);
        Assert.assertFalse(result);

    }
}
