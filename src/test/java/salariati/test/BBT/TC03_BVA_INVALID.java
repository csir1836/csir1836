package salariati.test.BBT;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import salariati.controller.EmployeeController;
import salariati.model.Employee;
import salariati.repository.implementations.IEmployeeRepositoryImpl;
import salariati.repository.interfaces.IEmployeeRepository;

import java.util.Arrays;
import java.util.List;

import static salariati.enumeration.DidacticFunction.ASISTENT;
import static salariati.enumeration.DidacticFunction.CONFERENTIAR;
import static salariati.enumeration.DidacticFunction.LECTURER;

public class TC03_BVA_INVALID {
    private IEmployeeRepository employeeRepository;
    private EmployeeController employeeController;

    @Before
    public void setUp() {
        employeeRepository = new IEmployeeRepositoryImpl();
        employeeController = new EmployeeController(employeeRepository);
    }

    @Test(expected = NumberFormatException.class)
    public void shouldBeUnOrdered(){
        Employee employee1 = new Employee("Caspriac","Dalia","2911002356881", CONFERENTIAR, "10%00");
        Employee employee2 = new Employee("Marian","Adela","2896656775656", ASISTENT, "90%0");
        Employee employee3 = new Employee("Popovici","Matei","1876565765656", LECTURER, "300");

        List<Employee> unorderedList = Arrays.asList(employee2, employee3, employee1);
        employeeController.sort(unorderedList);
    }
}
