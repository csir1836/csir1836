package salariati.test.lab03;


import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import salariati.controller.EmployeeController;
import salariati.model.Employee;
import salariati.repository.implementations.IEmployeeRepositoryImpl;
import salariati.repository.interfaces.IEmployeeRepository;

import java.util.List;

import static java.lang.Boolean.TRUE;
import static salariati.enumeration.DidacticFunction.ASISTENT;
import static salariati.enumeration.DidacticFunction.CONFERENTIAR;

public class ModifyEmployeeTest {
    private IEmployeeRepository employeeRepository;
    private EmployeeController employeeController;

    @Before
    public void setUp() {
        employeeRepository = new IEmployeeRepositoryImpl();
        employeeController = new EmployeeController(employeeRepository);
    }

    @Test
    public void employeeShouldNotBeModified1() {
        Employee oldEmployee = new Employee("Rete", "Mihaela", "2921221375121", CONFERENTIAR, "1234");
        Assert.assertEquals(oldEmployee.getFirstName(), "Rete");
        Assert.assertEquals(oldEmployee.getLastName(), "Mihaela");
        Assert.assertEquals(oldEmployee.getCnp(), "2921221375121");
        Assert.assertEquals(oldEmployee.getFunction(), CONFERENTIAR);
        Assert.assertEquals(oldEmployee.getSalary(), "1234");

        boolean result = employeeController.addEmployee(oldEmployee);
        Assert.assertTrue(result);

        Employee newEmployee = new Employee("Re$$te", "Mihaela", "2921241375121", CONFERENTIAR, "1234");
        Assert.assertEquals(newEmployee.getFirstName(), "Re$$te");
        Assert.assertEquals(newEmployee.getFunction(), CONFERENTIAR);

        employeeController.modifyEmployee(oldEmployee, newEmployee);
        employeeRepository.deleteEmployee(newEmployee);
    }

    @Test
    public void employeeShouldNotBeModified2() {
        Employee oldEmployee = new Employee("Rete", "Mihaela", "2921221375121", CONFERENTIAR, "1234");
        Assert.assertEquals(oldEmployee.getFirstName(), "Rete");
        Assert.assertEquals(oldEmployee.getLastName(), "Mihaela");
        Assert.assertEquals(oldEmployee.getCnp(), "2921221375121");
        Assert.assertEquals(oldEmployee.getFunction(), CONFERENTIAR);
        Assert.assertEquals(oldEmployee.getSalary(), "1234");

        boolean result = employeeController.addEmployee(oldEmployee);
        Assert.assertTrue(result);

        Employee newEmployee = new Employee("Rete", "Mihaela", "2921241375121", CONFERENTIAR, "1234");
        Assert.assertEquals(newEmployee.getFunction(), CONFERENTIAR);

        employeeController.modifyEmployee(oldEmployee, newEmployee);
        employeeRepository.deleteEmployee(newEmployee);
    }

    @Test
    public void shouldModifyEmployee(){
        Employee oldEmployee = new Employee("Chiorean","Adrian","1921221375121", CONFERENTIAR, "1234");
        Assert.assertEquals(oldEmployee.getFirstName(), "Chiorean");
        Assert.assertEquals(oldEmployee.getLastName(), "Adrian");
        Assert.assertEquals(oldEmployee.getCnp(), "1921221375121");
        Assert.assertEquals(oldEmployee.getFunction(), CONFERENTIAR);
        Assert.assertEquals(oldEmployee.getSalary(), "1234");

        boolean result = employeeController.addEmployee(oldEmployee);
        Assert.assertTrue(result);

        Employee newEmployee = new Employee("Chiorean","Adrian","1921221375121", ASISTENT, "1234");
        Assert.assertEquals(newEmployee.getFunction(), ASISTENT);

        employeeController.modifyEmployee(oldEmployee, newEmployee);

        List<Employee> employees = employeeController.getEmployeesList();
        for(Employee employee:employees){
            if(employee.equals(newEmployee)){
                Assert.assertTrue("modificat cu success", TRUE);
            }
        }
        employeeRepository.deleteEmployee(newEmployee);
    }

    @Test(expected = NullPointerException.class)
    public void shouldFailModifyEmployee(){
        Employee oldEmployee = new Employee("Chiorean","Adrian","1921221375121", CONFERENTIAR, "1234");
        boolean result = employeeController.addEmployee(oldEmployee);
        Assert.assertTrue(result);

        Employee newEmployee = new Employee("Rete","Mihaela","2921221375121", null, "1234");

        employeeController.modifyEmployee(oldEmployee, newEmployee);
    }
}
