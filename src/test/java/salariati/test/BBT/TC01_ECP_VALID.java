package salariati.test.BBT;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import salariati.controller.EmployeeController;
import salariati.model.Employee;
import salariati.repository.implementations.IEmployeeRepositoryImpl;
import salariati.repository.interfaces.IEmployeeRepository;

import static salariati.enumeration.DidacticFunction.ASISTENT;

public class TC01_ECP_VALID {
    private IEmployeeRepository employeeRepository;
    private EmployeeController employeeController;

    @Before
    public void setUp() {
        employeeRepository = new IEmployeeRepositoryImpl();
        employeeController = new EmployeeController(employeeRepository);
    }

    @Test
    public void shouldReturnTrueForValidEmployee() {
        Employee employee = new Employee("Caspriac", "Maria", "2921221375121", ASISTENT, "1000");
        Assert.assertEquals(employee.getFirstName(), "Caspriac");
        Assert.assertEquals(employee.getLastName(), "Maria");
        Assert.assertEquals(employee.getCnp(), "2921221375121");
        Assert.assertEquals(employee.getFunction(), ASISTENT);
        Assert.assertEquals(employee.getSalary(), "1000");

        boolean result = employeeController.addEmployee(employee);
        Assert.assertTrue(result);

        employeeRepository.deleteEmployee(employee);
    }
}
