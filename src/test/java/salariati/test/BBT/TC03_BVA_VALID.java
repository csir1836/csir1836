package salariati.test.BBT;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import salariati.controller.EmployeeController;
import salariati.model.Employee;
import salariati.repository.implementations.IEmployeeRepositoryImpl;
import salariati.repository.interfaces.IEmployeeRepository;

import java.util.Arrays;
import java.util.List;

import static salariati.enumeration.DidacticFunction.*;

public class TC03_BVA_VALID {
    private IEmployeeRepository employeeRepository;
    private EmployeeController employeeController;

    @Before
    public void setUp() {
        employeeRepository = new IEmployeeRepositoryImpl();
        employeeController = new EmployeeController(employeeRepository);
    }

    @Test
    public void shouldBeOrdered(){
        Employee employee1 = new Employee("Caspriac","Dalia","2911002356881", CONFERENTIAR, "1000");
        Employee employee2 = new Employee("Marian","Adela","2896656775656", ASISTENT, "900");
        Employee employee3 = new Employee("Popovici","Matei","1876565765656", LECTURER, "300");

        List<Employee> orderedList = Arrays.asList(employee1, employee2, employee3);
        List<Employee> unorderedList = Arrays.asList(employee2, employee3, employee1);
        employeeController.sort(unorderedList);
        Assert.assertEquals(orderedList,unorderedList);
    }
}
