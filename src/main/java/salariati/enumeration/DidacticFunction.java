package salariati.enumeration;

public enum DidacticFunction {
    ASISTENT,
    LECTURER,
    TEACHER,
    //todo:recently added
    CONFERENTIAR
}
