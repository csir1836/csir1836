package salariati.controller;

import java.text.ParseException;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import salariati.model.Employee;
import salariati.repository.interfaces.IEmployeeRepository;

public class EmployeeController {

    private IEmployeeRepository employeeRepository;

    public EmployeeController(IEmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    public boolean addEmployee(Employee employee) {
        return employeeRepository.addEmployee(employee);
    }

    public List<Employee> getEmployeesList() {
        return employeeRepository.getEmployeeList();
    }

    public void modifyEmployee(Employee oldEmployee, Employee newEmployee) {
        employeeRepository.modifyEmployee(oldEmployee, newEmployee);
    }

    public void deleteEmployee(Employee employee) {
        employeeRepository.deleteEmployee(employee);
    }

    public List<Employee> sort(List<Employee> employees) {
        List<Employee> list = employees;

        Comparator<Employee> comparator = Comparator.comparingInt((Employee p) -> Integer.parseInt(p.getSalary())).reversed()
                .thenComparingInt(p -> {
                    try {
                        return p.getVarsta();
                    } catch (ParseException e) {
                        e.printStackTrace();
                        return -1;
                    }
                });

        Collections.sort(list, comparator);
        return employees;
    }

//    public List<Employee> sortByVarsta(List<Employee> employees){
//        List<Employee> list = employees;
//        Collections.sort(list, new Comparator<Employee>() {
//            @Override
//            public int compare(Employee o1, Employee o2) {
//                try {
//                    if(o1.getVarsta() == o2.getVarsta()){
//                        return 1;
//                    }
//                } catch (ParseException e) {
//                    e.printStackTrace();
//                }
//                try {
//                    if(o1.getVarsta() > o2.getVarsta()){
//                        return 1;
//                    }
//                } catch (ParseException e) {
//                    e.printStackTrace();
//                }
//                return -1;
//            }
//        });
//        return employees;
//    }
}
