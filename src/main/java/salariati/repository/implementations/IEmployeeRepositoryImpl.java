package salariati.repository.implementations;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

import salariati.exception.EmployeeException;

import salariati.model.Employee;

import salariati.repository.interfaces.IEmployeeRepository;
import salariati.validator.EmployeeValidator;

public class IEmployeeRepositoryImpl implements IEmployeeRepository {

    private final String employeeDBFile = "C:\\Users\\Chira Paul\\vvss\\csir1836\\employeeDB\\employees.txt";
    private EmployeeValidator employeeValidator = new EmployeeValidator();

    @Override
    public boolean addEmployee(Employee employee) {
        if (employeeValidator.isValid(employee)) {
            //TODO: REMOVED NULL
            BufferedWriter bw;
            try {
                bw = new BufferedWriter(new FileWriter(employeeDBFile, true));
                bw.write(employee.toString());
                bw.newLine();
                bw.close();
                return true;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    @Override
    public void deleteEmployee(Employee employee) {
        // TODO Auto-generated method stub
        try {

            File inFile = new File(employeeDBFile);

            if (!inFile.isFile()) {
                System.out.println("Parameter is not an existing file");
                return;
            }

            //Construct the new file that will later be renamed to the original filename.
            File tempFile = new File(inFile.getAbsolutePath() + ".tmp");

            BufferedReader br = new BufferedReader(new FileReader(employeeDBFile));
            PrintWriter pw = new PrintWriter(new FileWriter(tempFile));

            String line = null;

            //Read from the original file and write to the new
            //unless content matches data to be removed.
            while ((line = br.readLine()) != null) {

                if (!line.trim().equals(employee.toString())) {

                    pw.println(line);
                    pw.flush();
                }
            }
            pw.close();
            br.close();

            //Delete the original file
            if (!inFile.delete()) {
                System.out.println("Could not delete file");
                return;
            }

            //Rename the new file to the filename the original file had.
            if (!tempFile.renameTo(inFile))
                System.out.println("Could not rename file");

        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void modifyEmployee(Employee oldEmployee, Employee newEmployee) {
        // TODO
        if (employeeValidator.isValid(newEmployee)) {
            boolean modifyNeeded = false;
            List<Employee> employees = getEmployeeList();
            for (Employee employee : employees) {
                if (employee.getCnp().equals(oldEmployee.getCnp()) && !oldEmployee.getFunction().equals(newEmployee.getFunction())) {
                    modifyNeeded = true;
                    employee.setFunction(newEmployee.getFunction());
                }
            }
            if (modifyNeeded) {
                deleteFileContent();
                for (Employee employee : employees) {
                    addEmployee(employee);
                }
            }
        }
    }

    @Override
    public List<Employee> getEmployeeList() {
        List<Employee> employeeList = new ArrayList<Employee>();

        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader(employeeDBFile));
            String line;
            int counter = 0;
            while ((line = br.readLine()) != null) {
                Employee employee;
                try {
                    employee = Employee.getEmployeeFromString(line, counter);
                    employeeList.add(employee);
                } catch (EmployeeException ex) {
                    System.err.println("Error while reading: " + ex.toString());
                }
            }
        } catch (FileNotFoundException e) {
            System.err.println("Error while reading: " + e);
        } catch (IOException e) {
            System.err.println("Error while reading: " + e);
        } finally {
            if (br != null)
                try {
                    br.close();
                } catch (IOException e) {
                    System.err.println("Error while closing the file: " + e);
                }
        }

        return employeeList;
    }


    @Override
    public List<Employee> getEmployeeByCriteria(String criteria) {
        List<Employee> employeeList = new ArrayList<Employee>();

        return employeeList;
    }

    private void deleteFileContent() {
        PrintWriter writer = null;
        try {
            writer = new PrintWriter(employeeDBFile);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        writer.print("");
        writer.close();
    }

}
